# Workshop plan and objectives

## Workshop overview
The workshop will take place over two days. Each day is quite autonomous from the other. 
- The goal of **day 1** is to draft the content of a **guidebook on software preservation** (with the ideat that the current [SWHAP guide](https://unesdoc.unesco.org/ark:/48223/pf0000371017) could be one section of this guidebook).  
    - **Morning**: identifiy the guidebook audience using Personnas
    - **Afternoon**: drafting a detailed table of content and identify ressources
- The goal of **day 2** is to work on how to best **present** the story of a legacy software in digital format, building up on the Software Stories tool
    - **Morning**: work on storyboards for given historical softwares
    - **Afternoon**: experiment with Software Stories and identify best practive or desired evolutions


## Day 1 - Drafting a comprehensive guidebook on Software Preservation

**9:30-10:45 - Workshop launch**

### Welcome word *(15min)*

⌚ 9:30  
👤 Roberto
:pencil: Slides?

### Workshop goals *(10 min)*

⌚ 9:45  
👤 Morane

- Thank you
- Preservation guide for Landmark Legacy Software
- Stories 

### Rules *(5 min)*

⌚ 9:55  
👤 Lunar

We expect everyone to behave in a **professional seminar**. That means you should treat everyone as a colleague and expect to be treated as such. If you feel uneasy at any point, please come talk to Mathilde, Morane, or myself.

Please make sure **everyone feels included and can participate in the discussions**. Especially, please watch out for any jargon. Be mindful and use a language that everyone around can understand. That goes for English or French, and also for tech speech. I suggest we use the “L sign”–L stands for “language”–anytime we need to signal that we have not understood something.

Let’s not turn our group discussions into ping-pongs. If it starts, please **agree to disagree and move on**. If it helps, try to understand what is you disagree on, and write it down for a future discussion on that topic. There is a posterboard on the wall to actually write these down if you want to keep track of them.

Finally, please **do your best to ensure that everyone has the chance to speak**. I know I am guilty of taking too much space myself. That means leaving some silence between turns. The advice is to raise only one idea at a time. And generally, try to speak at most 1/nth of the time. For example, in a group of ten, try to speak only one tenth of the time.

The schedule is pretty dense for these two days, but we did our best to account for proper breaks. Please **be on time** so we can make the best of our time together here. The pro-tip: if you need to go to the bathroom, or check your phone, or smoke a cigarette, do it at the start of the break rather than at the last minute before we start again.

### Getting to know each other *(15min)*

⌚ 10:00  
👤 Lunar

- Classements par:
    - noms
    - distance parcourue pour venir
    - organization
    - favorite place in Paris/place you want to visit

### Example of history preservation *(30 min)*

⌚ 10:15  
👤 Lunar

- Brainstorm: 3psn interview (interviewer, interviewee, note taking):
    - Talk about an example of history preservation that you found remarkable. Software is nice, but it can be about anything. Maybe it was in a museum, maybe it was in a book, maybe it was in a documentary…
    - Highlight approaches that worked out well, why did it work, which emotions, how… 
    - (agree on terminology)
    - (agree on the objectives)
    - Examples : museums, buildings, art pieces, exhibition, historical artefacts

It's an exercice to get in a good mindset. We won’t share the results. If you are curious, feel free to ask other attendees during the break!

### Break *(30 min)*

⌚ 10:45

- Use 5min of break to look at guide book selection

### Identifying the audience for a guidebook on software preservation *(1h15)*

⌚ 11:15  
👤 Morane
:pencil: post-its (brainstorm, check with Morane), A4 personas, A3 personas template

Text:
Good to have you back from the coffee break.
Thank you 
- connect workshop goal "creating a software preservation guide" and what can be the reasons to create such guide
...
- from the why to the for whom
- activity

Goal: get to a common set of personas, to help us better identify with our audience and understand their needs

- Brainstorm about the need for a guide (SWHAP guide, limits, potential improvements, introduce personas approach)
    - Why do we need such a guide? (5min)
    - What is a good documentation? (5min)
- Breakout time *(30min)*: 
    - what is my [persona](https://gitlab.softwareheritage.org/outreach/events/swhap-workshop-2023/-/blob/main/02_WorkshopMaterial/Personas.docx) expecting from such a guide. 
        - 5 tables (Mathilde to decide which personas)
        - 5min for people to discover the different personas and choose one
        - Then small groups
    - Fill in the [template](https://gitlab.softwareheritage.org/outreach/events/swhap-workshop-2023/-/blob/main/02_WorkshopMaterial/Personas.docx) 
- Presenting personas *(30min)*

### Lunch break *(1h30)*

⌚ 12:45

*Il Goto*

### Warm up *(15 min)*

⌚ 14:15  
👤 Lunar

*Isabelle Astic arriving*

Let’s count together.

### Building the table of content key sections

⌚ 14:30  
👤 Morane
:pencil: 1 color post-it for the center, then other colors for brainstorm, other color for final sections. 

Text:
- this morning we have discussed 5 different actors
- the personas are presented on your right.
- On the left, on the board, we will create together a mindmap - the center of this mindmap is `guide-book for legacy landmark software`
- Each on of you will get a

Goal: get to a detailed table of content for a comprehensive guidebook on software preservation

- Sticky notes brainstorm (each person playing a persona) *(45min)*
    - what are the key sections your persona would want to find in the guide book?  get to a common set of chapters, maybe some subchapters. Mindmap  (take 5 min to think)
    - Center: guidebook, then mindmap around it all together
    - Take some time to reorganize, make links, exclude some topics
        - Do we want to exclude some topics/use case/personnas?

### Break *(15min)*

⌚ 15:15

### Building subsections and key ideas *(2x45min)*

⌚ 15:30  
👤 Morane
:pencil: 5 giga post-its, post-its to comment.

Prep: each table with a chapter (even if more than 5)
- 40min initial writup
- (movements 5 min)
- 15min review
- (movements 5 min)
- 15min review
    - Someone should read it

- Divide groups per chapters
- What are the subsections of each chapter ? 
- What would be the key ideas of each chapter? What expertise, ressources, data, would you need to fill in each chapter? 
- Each chapter should be reviewed by a minimum of 2 groups 

### Putting in common *(25 min)*

⌚ 17:00  
👤 Morane

- Reading in order of chapters and sub-sections
- Comments, surprises, obvious miss

### Closing for the day *(5 min)*

⌚ 17:25  
👤 Morane

- Feelings if needed
- Restaurant info

### Restaurant

⌚ 19:00

## Day 2 - Telling the story of a software in digital format

### Warming up *(15 min)*

⌚ 9:30  
👤 Lunar

Walk to occupy the space. Break with someone close to you:
- How do you feel this morning?
- Something you enjoyed yesterday?
- Something you are looking for today?

### Building the storyboard for a legacy software *(1h15)*

⌚ 9:45  
👤 Lunar
:pencil: Building blocks (each x5), 5 giga post-its, sharpies, A4 software material

Goal: think of efficient ways to present the story of a software, in a **digital format**, to a broad audience

- Present activity (15 min)
- Working in groups *(1h)* 
    - A group per [software](https://gitlab.softwareheritage.org/outreach/events/swhap-workshop-2023/-/blob/main/02_WorkshopMaterial/Software%20stories%20material.odt?ref_type=heads)
    - Using the [building blocks](https://gitlab.softwareheritage.org/outreach/events/swhap-workshop-2023/-/blob/main/02_WorkshopMaterial/Buildingblocks.pdf?ref_type=heads) of a software story (digital), how would you tell the story of a given software? 
    - Build a storyboard on A2 sheet
        
Prompts: science museum, 


### Break *(20 min)*

⌚ 11:00

### Defining a common template *(1h)*

⌚ 11:20  
👤 Lunar
:pencil: 1 big paperboard

- Present to group the story-board *(30min)*
    - showcase the storyboards
    - space to note taking
- Get to a common template or identify best practices? *(30min)*
    - Let people move around  
    - identify commonalities and differences
    - YES AND...
    - One person taking notes - paper board (👤Mathilde)

### Group picture *(10 min)*

⌚ 12:20  
👤 Morane

### Lunch *(1h15)*

⌚ 12:30

### Warming up *(15 min)*

⌚ 14:00  
👤 Lunar

Pac-man game.

### The future of Software Stories *(1h25)*

⌚ 14:15  
👤 Morane
:pencil: 4 computers, 5 giga post-its, postits for questions, printed questions on A5

Goal: identify Software Stories best practice and potential evolutions

- Presenting sciences stories - Kenneth *(20min)*
- Divide into small groups with one computer per group
- Navigate in a ready-made Story - [Amaya](https://stories.k2.services/publisher/collections/50/embed/stories/Q455973?api-key=tN5X1y7O.2NhLogJVhLsYANuCtSP1FjHPZHA6TUBd) *(20min)*
    - Questions pool (post-its)
- Breakout per topic *(45min)*

Print questions on A5:
 - How to present the SWH moment? 
 - How to navigate in the story? 
 - What to feature in a timeline?
 - What do we want to share about software contributors?
 - How to make the stories more narrative?

**15:40-16:00 Break (20min)**

### Break *(20 min)*

⌚ 15:40

### Putting in common *(30 min)*

⌚ 16:00  
👤 Morane

- Each group has navigated in the Software Story of Amaya and collected questions.

### Next steps *(20 min)*

⌚ 16:30  
👤 Lunar
:pencil: Post-its

What are the next steps you would like to see? Which ones would you like to actually do? → using post-its

### Feedback about seminar *(20 min)*

⌚ 16:50  
👤 Lunar
:pencil: 

Go around. Unchallenged speech. Pépite et caillou.

### Conclusion *(5 min)*

⌚ 17:10  
👤 Roberto


