# List of workshop guests
 Total participants: 15

## Confirmed
- Kenneth Seals-Nutt	Science Stories
- Carlo Montangero	Pisa University
- Claude Gomez	Inria	
- Isabelle Astic	CNAM	(Arriving after lunch Day 1)
- Emmanuelle Bermes	Ecole des Chartes
- Camille Françoise	Ambassadeur SWH	
- Simon Phipps	Ambassadeur SWH
- Wendy Hagenmaier	SPN
- Jean-François	Abramatic (only Day 2)
- Gregory Miura	Université de Rennes, Collège logiciel	
- Sabrina Granger	SWH	
- Lunar	SWH	(facilitator)
- Mathilde Fichen	SWH	
- Morane Gruenpeter	SWH (facilitator)
- Roberto Di Cosmo	SWH	
