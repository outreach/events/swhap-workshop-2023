# Emails

## Invitation Emails (Sent june 23)

*Option1*
Cher(e) XXX,
Software Heritage organise deux jours d'ateliers opérationnels (type hackathon) autour de la question de la préservation et valorisation du logiciel historique, les 14 et 15 septembre 2023 de 9h à 18h au centre Inria Paris (2 Rue Simone IFF, 75012 Paris). Un dîner sera prévu le 14 au soir avec les participants.

La première journée aura pour but d'initier la rédaction d'un guide complet sur la conservation du logiciel, en s'appuyant notamment sur un premier guide technique publié dans le cadre de notre partenariat avec l'UNESCO. Pendant la deuxième journée nous nous demanderons comment raconter l'histoire d'un logiciel au grand public, en utilisant notamment les retours d'expérience de l'initiative Software Stories.

L'atelier réunira une dizaine de personnes de différents horizons (acteurs de la conservation et du patrimoine, chercheurs en informatique etc.) et nous serions heureux que vous puissiez vous joindre à nous pour ces deux jours de travail et de réflexion. A noter que les échanges se feront en anglais. Les productions réalisées durant l'atelier pourront être diffusées sous licence libre Creative Commons (CC-BY-SA) avec crédit aux contributeurs.

Afin de nous permettre une bonne organisation, pourriez-vous nous confirmer votre participation d'ici au 15 juin ?

*Option2*
Cher(e) XXX,

En octobre 2022 Software Heritage organisait ses premiers SWHAP Days, colloque de deux jours dédié à la conservation du logiciel et des codes sources.
Cette année nous optons pour deux jours d'ateliers opérationnels en commité restreint (type hachathon), les 14 et 15 septembre 2023, au centre Inria Paris (2 Rue Simone IFF, 75012 Paris). (Si pertinent de mentionner le lien avec Roberto, par ex Emmanuelle Bernes, Daniel Dardaillet, Stefano Penge:) Roberto Di Cosmo et l'équipe Software Heritage seraient ravis que vous puissiez vous joindre à nous.  

La première journée aura pour but d'initier la rédaction d'un guide complet sur la conservation du logiciel, en s'appuyant notamment sur un premier guide technique publié dans le cadre de notre partenariat avec l'UNESCO. Pendant la deuxième journée nous nous demanderons comment raconter l'histoire d'un logiciel au grand public, en utilisant notamment les retours d'expérience de l'initiative Software Stories.

L'atelier réunira une quinzaine de personnes de différents horizons (acteurs de la conservation et du patrimoine, chercheurs, ingénieurs etc.) et nous serions heureux que vous puissiez vous joindre à nous pour ces deux jours de travail et de réflexion. Les productions réalisées durant l'atelier pourront être diffusées sous licence libre Creative Commons (CC-BY-SA) avec crédit aux contributeurs. Les déjeuners seront pris en charge sur place et un dîner sera prévu le 14 au soir avec les participants. (Pour les ambassadeurs: ) Nous disposons d'un buget limité pour contribuer aux frais de déplacement et d'hébergement, merci de nous indiquer si vous souhaitez pouvoir en bénéficier.  

Afin de nous permettre une bonne organisation, pourriez-vous nous confirmer votre participation d'ici au 7 juillet ?

## Pre-workshop email (Sent July 21)

Dear participant,

We are delighted that you can join us on September 14th and 15th at Inria Paris for the SWHAP workshop. 

**To help us best prepare, can you please fill in this [quick survey](https://framaforms.org/swhap-workshop-14th-15th-of-september-1689152740) before September 4th?**

Here are also so more detailed information regarding the workshop:

### Where will the workshop take place?
- The Inria Paris research centre is located in the 12th arrondissement of Paris at 2 rue Simone Iff.
- The closest metro stations are 
    - Dugommier station - Line 6
    - Montgallet station - Line 8

Lunches will take place at Il Goto, a nearby restaurant.
Dinner will take place at a walkable distance from Inria Paris (venue to be confirmed).

### What is the workshop planning?

**Thursday September 14th: drafting a comprehensive guidebook on software preservation**
- 9:00-9:30: Welcome coffee
- 9:30-10:00: Workshop launch
- 10:00-13:00: Identifying the audience for a guidebook on software preservation
- 13:00-14:30: Lunch break
- 14:30-18:00: A detailed table of content for a guidebook on software preservation
- 19:00-21:00: Dinner

**Friday September 15th: presenting the history of legacy software**
- 8:30-9:00: Welcome coffee
- 9:00-12:30: Building the storyboard for a software story
- 12:30-14:00: Lunch break
- 14:00-16:30: The future of SoftwareStories
- 16:30-17:30: Wrapping up

If you forsee any planning constraints, please let us know.

### What do I need to bring?
You do not need to bring anything appart from yourself. You do not need a laptop. Food, snacks and drinks are taken care of. 

### License
All contributions to this workshop are under CC-BY-4

### Quick links
- SWH website: https://www.softwareheritage.org/
- SWH Code of conduct: https://gitlab.softwareheritage.org/swh/devel/swh-docs/-/blob/master/CODE_OF_CONDUCT.md
- Collect and Curate Legacy Code, the SWHAP Guide https://github.com/SoftwareHeritage/swhapguide/blob/master/SWHAP%40Pisa.pdf
- Software Stories, the Pisa and Inria collection: https://stories.softwareheritage.org/


Kind regards,

Mathilde

## Week-1 email 

[SWHAP WOrkshop] we are meeting next week!

Dear Wendy,

We are excited to welcome you next week to the SWHAP workshop in Paris.  

Final programme is available here:
https://gitlab.softwareheritage.org/outreach/events/swhap-workshop-2023/-/blob/main/2023-09-14-SWHAP-Program.pdf


We are waiting for you next Thursday at 9:00 for coffee at
Inria Paris
Salle Gille Kahn
2, rue Simone Iff, 75012
Paris

On Thursday 14th, dinner will take place at Terre Restaurant (23, rue de Montreuil, 75011 Paris) at 7pm.
Can you please let us know before coming Tuesday which main dish you would like to eat:
- Option 1: Poultry in two variations: grilled supreme with corn, stuffed tomato and savory.
- Option 2: Grilled white tuna from St Jean-de-Luz, beurre blanc, fennel and sage
- Option 3: Eggplant and zucchini caponata, smoked eggplant caviar, Sicilian condiments

Until then, please feel free to reach out if any question.

Thank you for participating, we look forward to collaborating with you!

Best wishes,




## After workshop email

Dear X

The Software Heritage team would like to thank you very much for attending and contributing to the SWHAP Workshop. We truly appreciate the effort and time you have consecrated to this event.

To help us identify what worked well, what could be improved and what our next steps could be, could you could please take 5 minutes to fill in this survey?
https://framaforms.org/swhap-workshop-evaluation-1695042510

We will prepare a first draft of the workshop proceedings shortly and will send them over in the coming weeks for final review. We are planning on creating a mailing list with all the workshop participants in order to easily share the proceedings and any workshop followup initiatives. Please let us know before this Friday if you do not wish to join the mailing list. 

Looking forward continuing our fruitful collaboration.

Kind regards,
Mathilde & SWHAP workshop team
