
# Survey sent out to participants (using FramaForm)

## Information that will be used on your badge and for workshop proceedings
- Full name
- Pronouns
- Affiliation or Organization
- ORCID (if available)

## Practical information
- Do you have any dietary restrictions?
- Will you be joining for dinner on September 14th?
- Do you consent to being photographed during the Workshop?

## About the workshop
- What specific outcomes or discussion topics will be most beneficial to you and the work you are doing?
- Do you have any other questions or concerns about the event?

