# The SWHAP Workshop 2023 Proceedings

In this folder we will collect all text elements from the 
SWHAP workshop held in Paris.

As a first stage, we will work on an external colaboratif
document that will be shared with the participants
after a first draft.

Here is the link to the shared document:
https://cryptpad.fr/pad/#/2/pad/edit/+NwrtC1pqkbGuPTK7s7I1PBE/

Here is a link towards the final LaTeX document:
https://www.overleaf.com/6838479364qwtvwrfdkrfw

Here is a link towards the HAL deposit
