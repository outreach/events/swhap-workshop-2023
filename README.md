# SWHAP Workshop 2023 
The SWHAP workshop is an internal workshop to collaborate
on a guide to presevre, curate and present landmark 
legacy software.

This open repository is to work on the materials during the
SWHAP workshop.

## Location
Centre Inria Paris
2 rue Simone Iff, 75012 Paris

- Day 1: Gilles Kahn meeting room
- Day 2: Jacques-Louis Lions meeting room

## Program
[Link to the program](https://gitlab.softwareheritage.org/outreach/events/swhap-workshop-2023/-/blob/main/2023-09-14-SWHAP-Program.pdf)

## Links
During the workshop, participants will use collaborative
note taking document accessible on hedgedoc 
- https://hedgedoc.softwareheritage.org/2023-swhap-workshop-session-1


